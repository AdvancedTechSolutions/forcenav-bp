/*
 * HomePage
 *
 * This is the first thing users see of our App, at the '/' route
 *
 * NOTE: while this component should technically be a stateless functional
 * component (SFC), hot reloading does not currently support SFCs. If hot
 * reloading is not a neccessity for you then you can refactor it and remove
 * the linting exception.
 */
import React from 'react';
import styles from './styles.css';
import { withRouter } from 'react-router';
import ForceNav from '../../components/ForceNav';
import Member from '../../components/Member';
import NavNode2 from '../../components/NavNode2';
import blueImg from './images/blue_circle.png';
import purpleImg from './images/purple_circle.png';


class HomePage extends React.Component { 
  constructor(props) {
    super(props);
  }

  
  render() {
    return (
      <div className={styles.frame} >
        <div className={styles.container} >
          <Member id="root" className={styles.node} text="Member" />
          <NavNode2 id="node1" className={styles.node} backgroundImage={blueImg} text="1" />
          <NavNode2 id="node2" className={styles.node} backgroundImage={blueImg} text="2" />
          <NavNode2 id="node3" className={styles.node} backgroundImage={blueImg} text="3" />
          <NavNode2 id="node4" className={styles.node} backgroundImage={blueImg} text="4" />
        </div>
        <ForceNav
          showFPS
        />
      </div>
    );
  }
}


export default withRouter(HomePage);