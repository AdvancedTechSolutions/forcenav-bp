/**
 *
 * Img.react.js
 *
 * Renders an image, enforcing the usage of the alt="" tag
 */

import React, { PropTypes } from 'react';
import styles from './styles.css';
import Img from '../Img';

export default class Member extends React.Component { // eslint-disable-line react/prefer-stateless-function
  //const className = props.className ? props.className : styles.search;
  constructor(props) {
    super(props);
    this.className = props.className ? props.className + ' ' + styles.container : styles.container;
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick = (e) => {
    if (this.props.onClick) {
      this.props.onClick(e);
    }
  }

  render() {
    return (
      <div id={this.props.id} className={this.className} onClick={this.handleClick}>
        <div className={styles.text}><p>{this.props.text}</p><p>{this.props.text2}</p></div>
      </div>
    );
  }
}

// We require the use of src and alt, only enforced by react in dev mode
// SearchBox.propTypes = {
//   src: PropTypes.string.isRequired,
//   alt: PropTypes.string.isRequired,
//   className: PropTypes.string,
// };

