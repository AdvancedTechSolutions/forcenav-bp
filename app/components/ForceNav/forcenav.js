var __dx;
var __dy;
var __scale=0.5;
var __recoupLeft, __recoupTop;
var __dragx;
var __dragy;
var __dragTime;

Math.linearTween = function (t, b, c, d) {
  return c - c*t/d + b;
};

Math.linearTweenOut = function (t, b, c, d) {
  return c*t/d + b;
};

Math.easeOutQuad = function (t, b, c, d) {
  t /= d;
  return c-c * t*(t-2) + b;
};

var forcenav = {
  connectionDistance: 240,
  connectionForce: 200,
  chargeForce: 60,
  chargeDistance: 400,
  originForce: .06,
  enableOriginForce: false,
  maxSpeed: 75,
  friction: .1,
  nodeData: '',
  tree: null,
  nodes: {},
  connections: [],
  selectedNodeId: '',
  activeNodes: {},
  activeConnections: [],
  prevTime: new Date(),
  framesThisSecond: 0,
  fps: 60,
  lastFpsUpdate: new Date().getTime(),
  deltaTime: 0,
  simulationTimestep: 1000/60,
  frameDelta: 0,
  numUpdateSteps: 0,
  self: this,
  isEnabled: true,
  firstRender: true,
  renderCount: 0,
  origin: null,
  momentum: null,
  isRunning: false,

  init: function() {
    this.prevTime = new Date();
    this.tree = JSON.parse(this.nodeData);
    this.nodes = {};
    this.connections = [];
    this.selectedNodeId = '';
    this.nodes[this.tree.id] = {
      id: this.tree.id,
      force: {x:0, y:0}, 
      children: [],
      followers: [],
      clickable: typeof this.tree.clickable === 'undefined' ? false : this.tree.clickable,
      element: $('#' + this.tree.id),
      x: 0,
      y: 0,
      center: {x:0, y:0}
    };
    this.nodify(this.tree.id);
    this.selectedNodeId = this.tree.id;
    var x = $(window).width() / 2;
    var y = $(window).height() / 2;
    $('#' + this.selectedNodeId).offset({ top: y, left: x });

    this.parseTreeRecursive(this.tree);
    console.log(this.nodes);
    console.log(this.connections);
    this.refreshNodes();
    console.log("active nodes", this.activeNodes);
  },

  update: function() {
    
    // Request the next rendering event
    requestAnimationFrame(forcenav.update);

    // Calculate the rendering time
    var curTime = new Date();
    forcenav.deltaTime = curTime.getTime() - forcenav.prevTime.getTime(); 
    forcenav.prevTime = curTime;

    // Calculate the FPS
    if (curTime > forcenav.lastFpsUpdate + 1000) {
      forcenav.fps = 0.25 * forcenav.framesThisSecond + 0.75 * forcenav.fps;
      $('#fps').text(Math.ceil(forcenav.fps));
      forcenav.lastFpsUpdate = curTime.getTime();
      forcenav.framesThisSecond = 0;
    }
    forcenav.framesThisSecond++;

    if (!forcenav.isEnabled) return;
    if (forcenav.isRefreshing == true) return;

    forcenav.numUpdateSteps = 0;
    while(forcenav.deltaTime >= forcenav.simulationTimestep) {
      forcenav.updateForces(forcenav.simulationTimestep);
      forcenav.deltaTime -= forcenav.simulationTimestep;

      if (++forcenav.numUpdateSteps >= 240) {
        break;
      }
    }

    // Move Objects
    forcenav.updateNodePositions();
    forcenav.updateConnections();
  },

  run: function() {
    if (this.isRunning) return;
    this.isRunning = true;
    this.self = this;
    this.init();
    this.wireEvents();
    this.update();
  },

  wireEvents: function() {
    for (var key in this.nodes) {
      if (this.nodes[key].clickable === true) {
        $('#' + key).css('position', 'absolute');
        $('#' + key).click(function () {
          if ($(this).hasClass('dragging')) {
              $(this).removeClass('dragging');
          } else {
              forcenav.selectedNodeId = $(this).attr('id');
              forcenav.refreshNodes();
          }
        });
      }
    };

    $(".jb_draggable").draggable({ containment: $('#' + forcenav.selectedNodeId).parent(), delay: 100, distance: 0 },{
      start: function(event, ui) {
        $(this).data('moving', true);
        $(this).addClass('dragging');
        //$(this).css("position", "relative");
        $('#' + forcenav.selectedNodeId).data('isdragging', true);
        //resize bug fix ui drag
        var left = parseInt( $( this ).css( 'left' ), 10 );
        left = isNaN( left ) ? 0 : left;
        var top = parseInt( $( this ).css( 'top' ), 10 );
        top = isNaN( top ) ? 0 : top;
        __recoupLeft = left - ui.position.left;
        __recoupTop = top - ui.position.top;
      },
      drag: function(event, ui) {
        //resize bug fix ui drag `enter code here`
        __dx = ui.position.left - ui.originalPosition.left;
        __dy = ui.position.top - ui.originalPosition.top;

        ui.position.left = ui.originalPosition.left + ( __dx);
        ui.position.top = ui.originalPosition.top + ( __dy );
        //
        ui.position.left += __recoupLeft;
        ui.position.top += __recoupTop;

        var left = parseInt( $( this ).css( 'left' ), 10 );
        left = isNaN( left ) ? 0 : left;
        var top = parseInt( $( this ).css( 'top' ), 10 );
        top = isNaN( top ) ? 0 : top;
        __dragx = left;
        __dragy = top;
        __dragTime = new Date().getTime();
      },
      stop: function(event, ui) {
        $(this).data('moving', false);

        var left = parseInt( $( this ).css( 'left' ), 10 );
        left = isNaN( left ) ? 0 : left;
        var top = parseInt( $( this ).css( 'top' ), 10 );
        top = isNaN( top ) ? 0 : top;

        var finalTime = new Date().getTime();

        var time = ((finalTime - __dragTime) / (1000 / 60));
        var x = (left - __dragx) * time;
        var y = (top - __dragy) * time;

        //if (finalTime - __dragTime > 200) {
          forcenav.momentum = {x: x, y: y};
          console.log('momentum=', forcenav.momentum);
        //}

        //$(this).css("position", "absolute");
        $('#' + forcenav.selectedNodeId).data('isdragging', false);
      },
      create: function ( event, ui ) {
        $( this ).attr( 'oriLeft', $( this ).css( 'left' ) );
        $( this ).attr( 'oriTop', $( this ).css( 'top' ) );
      }
    });
  },

  destroy() {
    for (var variableKey in this.activeNodes){
        if (this.activeNodes.hasOwnProperty(variableKey)){
            delete this.activeNodes[variableKey];
        }
    }
    this.activeConnections = [];
    this.connections.forEach(function(connection) {
      console.log("removing: " + connection.parentId + '-' + connection.childId);
      var elem = document.getElementById(connection.parentId + '-' + connection.childId);
      elem.remove();
      $('#' + connection.parentId + '-' + connection.childId).remove();
    });
  },

  parseTreeRecursive: function(parent) {
    if (parent.children) {
      parent.children.forEach(function(child) {
        this.nodes[child.id] = {
          id: child.id,
          parentId: parent.id, 
          force: {x:0, y:0}, 
          children: [], 
          followers: [],
          clickable: typeof child.clickable === 'undefined' ? false : child.clickable,
          element: $('#' + child.id),
          x: 0,
          y: 0,
          center: {x:0, y:0}
        };
        if (child.origin) {
          this.nodes[child.id].origin = {x: child.origin.x, y: child.origin.y}
        }
        this.nodes[parent.id].children.push(child.id);
        this.nodify(child.id);
        this.connections.push({parentId: parent.id, childId: child.id});
        var itm = document.getElementById("linetemplate");
        var cln = itm.cloneNode(true);
        cln.id = parent.id + '-' + child.id;
        $('#' + this.tree.id).parent().append(cln);

        //document.getElementsByTagName('body')[0].appendChild(cln);
        console.log("creating: " + parent.id + '-' + child.id);
        this.parseTreeRecursive(child);
      }, this);
    }

    if (parent.followers) {
      parent.followers.forEach(function(follower) {
        this.nodes[parent.id].followers.push(follower.id);
      }, this);
    }
  },

  nodify: function(id) {
    $('#' + id).addClass("jb_draggable");
  },

  refreshNodes: function() {
    this.isRefreshing = true;
    var prevActiveNodes = jQuery.extend(true, {}, this.activeNodes);
    var prevConnections = this.activeConnections.slice(0);

    for (var variableKey in this.activeNodes){
        if (this.activeNodes.hasOwnProperty(variableKey)){
            delete this.activeNodes[variableKey];
        }
    }

    this.activeConnections = [];
    this.activeNodes[this.selectedNodeId] = this.nodes[this.selectedNodeId];
    if (this.nodes[this.selectedNodeId].parentId) {
      this.activeNodes[this.nodes[this.selectedNodeId].parentId] = this.nodes[this.nodes[this.selectedNodeId].parentId];
    }
    this.nodes[this.selectedNodeId].children.forEach(function(nodeId) {
      this.activeNodes[nodeId] = this.nodes[nodeId];
    },this);

    // this.activeConnections.forEach(function(connection) {
    //   $('#' + connection.parentId + '-' + connection.childId).hide();
    // });

    for (let key in this.nodes) {
      this.nodes[key].force = {x:0,y:0}
      let node = this.nodes[key];
      if (this.activeNodes[key]) {
        node.followers.forEach(function(follower) {
          $('#' + follower.id).show();
        });
        if (!prevActiveNodes[key]) {
          $('#' + key).show();
        } else {
          console.log('found previous');
        }
      } else {
        node.followers.forEach(function(follower) {
          $('#' + follower.id).hide();
        });
        $('#' + key).hide();
      }
    }

    this.connections.forEach(function(connection) {
      if (this.activeNodes[connection.parentId] && this.activeNodes[connection.childId]) {
        this.activeConnections.push(connection);
        //$('#' + connection.parentId + '-' + connection.childId).css('width', '0px');
        //$('#' + connection.parentId + '-' + connection.childId).css('top', '-2000px');
        $('#' + connection.parentId + '-' + connection.childId).show();
      } else {
        $('#' + connection.parentId + '-' + connection.childId).hide();
      }
      //$('#' + connection.parentId + '-' + connection.childId).hide();
    }, this);

    this.isRefreshing = false;
  },

  getDistance: function(x1,y1,x2,y2) {
    return Math.sqrt((x1-x2)*(x1-x2) + (y1-y2)*(y1-y2));
  },

  //var radianConversion = (180/Math.PI);
  getAngleDegrees: function(x1,y1,x2,y2) {
    return Math.atan2((y1-y2), (x1-x2)) *  (180/Math.PI);
  },

  getAngleRadians: function(x1,y1,x2,y2) {
    return Math.atan2((y1-y2), (x1-x2));
  },

  getChargeForce: function(x1,y1,x2,y2) {
    var distance = this.getDistance(x1,y1,x2,y2);
    if (distance > this.chargeDistance) {
      return {x:0,y:0};
    }
    var angle = this.getAngleRadians(x1,y1,x2,y2);
    var x = Math.linearTween(distance, 0, this.chargeForce, this.chargeDistance) * Math.cos(angle);
    var y = Math.linearTween(distance, 0, this.chargeForce, this.chargeDistance) * Math.sin(angle);

    // Gently move objects around each other if they are stuck
    var force = {x:x + Math.random() * .001, y:y + Math.random() * .001};

    var normForce = {x:force.x/(distance + .001), y:force.y/(distance + .001)};
    force = {x:normForce.x * this.maxSpeed, y:normForce.y * this.maxSpeed};

    return force;
  },

  getConnectionForce: function(x1,y1,x2,y2) {
    var distance = this.getDistance(x1,y1,x2,y2);
    var angle = this.getAngleRadians(x1,y1,x2,y2);

    var x = Math.linearTween(distance, 0, this.connectionForce, this.connectionDistance) * Math.cos(angle);
    var y = Math.linearTween(distance, 0, this.connectionForce, this.connectionDistance) * Math.sin(angle);

    var force = {x:-x, y:-y};

    var normForce = {x:force.x/(distance + .001), y:force.y/(distance + .001)};
    force = {x:normForce.x * this.maxSpeed, y:normForce.y * this.maxSpeed};

    return force;
  },

  getOriginForce: function(x1,y1,x2,y2) {
    var distance = this.getDistance(x1,y1,x2,y2);
    var angle = this.getAngleRadians(x1,y1,x2,y2);
    
    var x = Math.easeOutQuad(distance, 0, this.originForce, 1) * Math.cos(angle);
    var y = Math.easeOutQuad(distance, 0, this.originForce, 1) * Math.sin(angle);

    force = {x:x, y:y};

    var normForce = {x:force.x/(distance + .001), y:force.y/(distance + .001)};
    
    force = {x:normForce.x, y:normForce.y};

    return force;
  },

  updateForces: function(delta) {
    // Reset force values
    var nodeList = [];
    for (let key in this.activeNodes) {
        let node = this.nodes[key];
        nodeList.push(key);

        node.force.x = 0;
        node.force.y = 0;

        let os = node.element.offset();
        node.halfWidth = node.element.width()/2;
        node.halfHeight = node.element.height()/2;
        node.x = os.left;
        node.y = os.top;
        node.center = {x: node.x + node.halfWidth, y: node.y + node.halfHeight};

    }

    // Calculate Charge Forces
    for (var i=0;i<nodeList.length-1;i++) {
      let key = nodeList[i];
      let node1 = this.nodes[key];
        
      for (var j=i+1;j<nodeList.length;j++) {
        let key2 = nodeList[j];
        let node2 = this.nodes[key2];

        if (key == key2) continue;

        let force = this.getChargeForce(node1.center.x,node1.center.y,node2.center.x,node2.center.y);

        node1.force.x += force.x;
        node1.force.y += force.y;
        node2.force.x -= force.x;
        node2.force.y -= force.y;
      }
    }

    
    // Calculate Connection Forces
    this.activeConnections.forEach(function(connection) {
      //console.log(connection);
      let parent = this.nodes[connection.parentId];
      let child = this.nodes[connection.childId];

      let force = this.getConnectionForce(parent.center.x,parent.center.y,child.center.x,child.center.y);

      parent.force.x -= force.x;
      parent.force.y -= force.y;
      child.force.x += force.x;
      child.force.y += force.y;
    }, this);

    {
      // Calculate Momentum Forces
      let child = this.nodes[this.selectedNodeId];
      let element = child.element;
      if (element.data('isdragging') !== true) {

        if (this.momentum) {
          // Apply friction
          if (this.momentum.x > 0) {
            this.momentum.x -= this.friction;
            if (this.momentum.x < 0) this.momentum.x = 0;
          } else if (this.momentum.x < 0) {
            this.momentum.x += this.friction;
            if (this.momentum.x > 0) this.momentum.x = 0;
          }
          if (this.momentum.y > 0) {
            this.momentum.y -= this.friction;
            if (this.momentum.y < 0) this.momentum.y = 0;
          } else if (this.momentum.y < 0) {
            this.momentum.y += this.friction;
            if (this.momentum.y > 0) this.momentum.y = 0;
          }

          child.force.x = this.momentum.x;
          child.force.y = this.momentum.y;


        }

        // Calculate Centering Forces
        let container = element.parent();
        let x2 = container.offset().left + container.width() / 2;
        let y2 = container.offset().top + container.height() / 2;
        
        if (this.origin != null) {
          if (this.origin.x) x2 = this.origin.x;
          if (this.origin.y) y2 = this.origin.y;
        }

        //console.log(x1,y1,x2,y2);
        let force = this.getOriginForce(child.center.x,child.center.y,x2,y2);
        //console.log('origin force', force);
        child.force.x += force.x;
        child.force.y += force.y;
      }
    }

    // Calculate Origin Force
    if (this.enableOriginForce == true) {
      for (let key in this.activeNodes) {
        let node = this.nodes[key];

        if (node.origin) {
          let force = this.getOriginForce(node.center.x,node.center.y,node.origin.x,node.origin.y);

          node.force.x += force.x;
          node.force.y += force.y;
        }
      }
    }

    // Adjust force for frame rate
    for (let key in this.activeNodes) {
        let node = this.nodes[key];

        node.force.x *= 16.7 / delta;
        node.force.y *= 16.7 / delta;

        if (Math.abs(node.force.x) < 0.005) node.force.x = 0;
        if (Math.abs(node.force.y) < 0.005) node.force.y = 0;
    }

  },

  updateNodePositions: function() {
    let container = this.nodes[this.selectedNodeId].element.parent();
    let minX = container.offset().left;
    let minY = container.offset().top;
    let maxX = minX + container.width();
    let maxY = minY + container.height();

    for (let key in this.activeNodes) {
        let node = this.nodes[key];
        let nodeElement = node.element;
        
        if (nodeElement.data('moving') !== true) {
          let x = node.x;
          let y = node.y;
          
          if (x + node.force.x < minX) {
            console.log ('left side: ' + node.id + ' : ' + this.selectedNodeId);
            if (node.id == this.selectedNodeId && this.momentum) {
              this.momentum.x = 0;
            }
            
            if (!isFinite(node.force.x)) node.force.x = this.maxSpeed;
            node.force.x -= (x + node.force.x - 1) - minX;
          } else if (x + node.force.x + node.halfWidth * 2 > maxX) {
            if (node.id == this.selectedNodeId && this.momentum) this.momentum.x = 0;
            if (!isFinite(node.force.x)) node.force.x = -this.maxSpeed;
            node.force.x += (maxX - (x + node.force.x + node.halfWidth*2)) - 1;
          }

          if (y + node.force.y < minY) {
            if (node.id == this.selectedNodeId && this.momentum) this.momentum.y = 0;
            if (!isFinite(node.force.y)) node.force.y = this.maxSpeed;
            node.force.y -= (y + node.force.y - 1) - minY;
          } else if (y + node.force.y + node.halfHeight * 2 > maxY) {
            if (node.id == this.selectedNodeId && this.momentum) this.momentum.y = 0;
            if (!isFinite(node.force.y)) node.force.y = -this.maxSpeed;
            node.force.y += (maxY - (y + node.force.y + node.halfHeight*2)) - 1; 
          }

          nodeElement.transit({x: '+=' + node.force.x, y: '+=' + node.force.y}, 0, 'ease');
        }
    }
  },

  updateConnections: function() {

    this.activeConnections.forEach(function(connection) {
      
      let parent = this.nodes[connection.parentId];
      let child = this.nodes[connection.childId];

      let x1 = parent.center.x;
      let y1 = parent.center.y;
      let x2 = child.center.x;
      let y2 = child.center.y;

      let distance = this.getDistance(parent.center.x,parent.center.y,child.center.x,child.center.y);
      let angle = this.getAngleDegrees(parent.center.x,parent.center.y,child.center.x,child.center.y)
      if(angle >= 90 && angle < 180){
        y1 = y2;
      }
      if(angle > 0 && angle < 90){
        x1 = x2;
        y1 = y2;
      }
      if(angle <= 0 && angle > -90){
        x1 = x2;
      }

      let elem = $('#' + connection.parentId + "-" + connection.childId);
      elem.css({
        'transform' : 'rotate(' + angle + 'deg)',
        'width' : distance + 'px'
      });
      
      elem.offset({top: y1, left: x1}, 0, 'ease');
    }, this);
  }
};

window.forcenav = forcenav;