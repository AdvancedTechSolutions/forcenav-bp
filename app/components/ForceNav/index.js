/**
 *
 * ForceNav.js
 *
 * Adds Force Navigation to your project
 */

import React, { PropTypes } from 'react';
import styles from './styles.css';
import './jQueryRotateCompressed.js';
import './jquery.ui.touch.js';
import './forcenav.js?1386';
import nodeData from './data.json';

export default class ForceNav extends React.Component {
  constructor(props) {
    super(props);
    this.forcenav = window.forcenav;
    this.updateSettings(props);
  }

  componentDidMount() {
    console.log('FORCENAV:Initializing');
    this.forcenav.nodeData = JSON.stringify(nodeData);

    this.forcenav.run();
  }

  componentWillUnmount() {
    console.log('FORCENAV: Destroy');
    this.forcenav.destroy();
  }

  componentWillReceiveProps(newProps) {
    this.updateSettings(newProps);
  }

  updateSettings(props) {
    this.forcenav.enableOriginForce = props.enableOriginForce;
    if (props.connectionDistance) this.forcenav.connectionDistance = props.connectionDistance;
    if (props.connectionForce) this.forcenav.connectionForce = props.connectionForce;
    if (props.chargeForce) this.forcenav.chargeForce = props.chargeForce;
    if (props.chargeDistance) this.forcenav.chargeDistance = props.chargeDistance;
    if (props.originForce) this.forcenav.originForce = props.originForce;
    if (props.enableOriginForce) this.forcenav.enableOriginForce = props.enableOriginForce;
    if (props.maxSpeed) this.forcenav.maxSpeed = props.maxSpeed;
    if (props.friction) this.forcenav.friction = props.friction;
    this.forcenav.origin = props.center;
  }

  render() {

    return (
      <div>
        <div id="linetemplate" className={styles.line}></div>
        {this.props.showFPS ? <div className={styles.fpsContainer} ><div className={styles.fpsLabel}>FPS: </div><div id="fps" className={styles.fps} >60</div></div> : null }
        {this.props.enableOriginForce ? <div>
          <div className={styles.target1}>1</div>
          <div className={styles.target2}>2</div>
          <div className={styles.target3}>3</div>
          <div className={styles.target4}>4</div>
        </div> : null }
      </div>
    );
  }
}