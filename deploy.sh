#!/usr/bin/env bash
cd build
cp index.html 200.html
cp ../app/favicon.ico favicon.ico
echo -e '!node_modules/\n!node_modules' > .surgeignore
surge --domain forcenav.surge.sh