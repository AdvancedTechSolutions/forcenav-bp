#**ForceNav**

ForceNav is a force-based navigation engine.  It uses real-time physics calculations to provide interactive navigation to your applications.

[Live Demo](http://forcenav.surge.sh)

###Getting Started

####React Applications

Using ForceNav in your React Applications has never been easier.  Follow these simple steps to get up and running quickly:

 1.  Import/Include the ForceNav react component 
 2.  Add an instance of the component to your page
 2.  Add html ids to each object you would like controlled by ForceNav
 3.  Create a simple JSON document to describe the relationships of your objects
 
>ForceNav will automatically attach itself to your React components and take over their positioning and movement.

Sample:

    import React from 'react';
    import ForceNav from '/ForceNav';
    import Node from '/components/Node';
    
    class MyApp extends React.Component { {
      constructor(props) {
        ...
      }
      
      render() {
        return (
          <div>
            <ForceNav />
            <Node id="root" />
            <Node id="node1" />
            <Node id="node2" />
            <Node id="node3" />
            <Node id="node4" />
        </div>
        );
    }
  }
  
  export default MyApp;
      
JSON Example:

    forceNav.nodeData = `{
      "id": "root",
      "clickable": true,
      "children": [
        {
          "id": "node1",
          "clickable": true
        },
        {
          "id": "node2",
          "clickable": true
        },
        {
          "id": "node3",
          "clickable": true
        },
        {
          "id": "node4",
          "clickable": true
        }
      ]
    }`;

####Plain Ol' HTML Applications

ForceNav can also be used in any javascript application:

1.  Add the ForceNav javascript tag to your page
2.  Add html ids to each element you would like to be controlled by ForceNav
3.  Create a simple JSON document to describe the relationships of your objects

>ForceNav will automatically attach itself to your html elements and take over their positioning and movement.  

###Forces

ForceNav supports 5 different forces:

 - Connection Force - the force that holds connected nodes together.
 - Charge Force - the force that repels one node from another.
 - Centering Force - this force moves the selected node towards the center.
 - Friction Force - as nodes move around, they are constantly slowed by this force.
 - Momentum Force - this force keeps nodes moving even if no force is applied.

###Features

 - Supports standard React components - no need for special props or state
 - Automatically adds drag and click events to target objects
 - Forces can be adjusted to fit the application
 - Nodes are automatically kept inside the parent element
 - Supports centering for the selected node
 - (Coming Soon) Supports "ideal positioning" for all nodes - this allows you to specify a desired location for each node.  ForceNav will move nodes to that position if possible.
 - Touch-enabled for mobile device support
 - Supports node followers - this allows you to specify more than one element to move along with the primary node.

###jQuery
ForceNav currently uses jQuery to simplify several features including css selection, drag, click, and rotate.  Work is being done to remove this dependency on the full jQuery library.

###To-Do

 - Add rendering control settings as Props of the ForceNav react component.
 - Add support for "ideal positioning" of nodes.
 - Add support for multi-level connections.  
 - Remove jQuery dependency